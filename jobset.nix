{ nixpkgs ? <nixpkgs>, supportedSystems ? [ "x86_64-linux" ] }:

let

  inherit (import ./. { pkgs = import nixpkgs {}; }) mkJobset;

in

mkJobset {
  jobsetPrefix = "nur-personal";

  jobset = { mapTestOn, linux, ... }: mapTestOn {
    gianas-return = linux;
    autorandr = linux;
    sudo = linux;
    hydra = linux;
    nur-personal-library-tests = linux;
    php = linux;
    nur-personal-vm-tests.hydra = linux;
    nix = linux;
    #rustc = linux;
    i3status-rust = linux;
  };

  sources = [
    { inherit supportedSystems;
      channel = "release-20.03";
      trackBranches = true;
      upstream = "Ma27";
    }
    { inherit supportedSystems;
      channel = "release-20.03";
      trackBranches = true;
      upstream = "Ma27";
    }
    { inherit supportedSystems;
      channel = "19.03";
    }
  ];

  vmTests = {
    #hydra = (import ./modules/tests/hydra.nix { }).test;
  };

  libraryTests = [
    #./tests/test-checkout-nixpkgs.nix
    #./tests/test-containers.nix
    #./tests/test-mkjob.nix
  ];
}
